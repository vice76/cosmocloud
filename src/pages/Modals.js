import React, { useState } from "react";
import ModalCards from "../components/ModalCards";
import ModalContainer from "../components/ModalContainer";

function Modals() {
  const [showModal, setShowModal] = useState(false);
  const openDialog = () => setShowModal(!showModal);

  return (
    <>
      <div className="flex flex-col ">
        <div className=" m-4 flex justify-between">
          <div className="">
            <h4 className="text-2xl font-bold text-lime-500 p-2">Modals</h4>
          </div>
          <div className="">
            <button
              className="font-bold text-sm bg-lime-500 px-8 py-4 m-2 leading-3 text-white rounded-md"
              onClick={openDialog}
            >
              Create
            </button>
          </div>
        </div>
        <div>
          <ModalCards></ModalCards>
          <ModalCards></ModalCards>
          <ModalCards></ModalCards>
        </div>
      </div>
      {showModal && <ModalContainer openDialog={openDialog}></ModalContainer>}
    </>
  );
}

export default Modals;
