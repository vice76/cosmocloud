const awsexports = {
  Auth: {
    // REQUIRED - Amazon Cognito Region
    region: "ap-northeast-1",

    // OPTIONAL - Amazon Cognito User Pool ID
    userPoolId: "ap-northeast-1_Q76s9CVxc",

    // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
    userPoolWebClientId: "5rtlcavmv7uidilg7mk2l41ga7",
    // add the new key

    // OPTIONAL - This is used when autoSignIn is enabled for Auth.signUp
    // 'code' is used for Auth.confirmSignUp, 'link' is used for email link verification
    signUpVerificationMethod: "code", // 'code' | 'link'
    // authenticationFlowType: "ALLOW_USER_PASSWORD_AUTH",
  },
};

export default awsexports;
