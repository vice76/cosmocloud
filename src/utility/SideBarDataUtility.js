const SidebarData = [
  {
    title: "Modals",
    path: "/modals",
    icon: "empty",
  },
  {
    title: "API",
    path: "/api",
    icon: "empty",
  },
];
export default SidebarData;
