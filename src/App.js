import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Amplify } from "aws-amplify";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import Landingpage from "./pages/Landingpage";
import Modals from "./pages/Modals";
import Api from "./pages/Api";
import Dashboard from "./components/Dashboard";
import awsexports from "./utility/SessionUtility";
import Confirmation from "./components/Confirmation";
import FormPage from "./components/FormPage";
import ProtectedRoute from "./components/guards/ProtectedRoutes";
Amplify.configure(awsexports);

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />}></Route>
          {/* <Route path="/Login" element={<Login />}></Route> */}
          <Route path="/SignUp" element={<SignUp />}></Route>
          <Route path="/Dashboard" element={<Dashboard />}></Route>
          <Route path="/Confirmation" element={<Confirmation />}></Route>
          <Route path="/comingsoon" element={<Landingpage />}></Route>
          <Route path="/formpage" element={<FormPage />}></Route>
          <Route path="/api" element={<Api />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
