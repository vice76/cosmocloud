import React from "react";

function ModalCards() {
  return (
    <div className="flex justify-between m-4 p-8 w-100 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-white-800 dark:border-gray-300">
      <div className="">
        <a href="#">
          <h3 className="mb-2 text-xl font-medium tracking-tight text-gray-900 dark:text-black">
            Modal 1
          </h3>
        </a>
      </div>
      <div className="">
        <a href="#">
          <h2 className=" mb-2 text-xl font-medium tracking-tight text-gray-900 dark:text-black">
            Created : {Date.now()}
            {/* creation of card timing */}
          </h2>
        </a>
      </div>
    </div>
  );
}

export default ModalCards;
