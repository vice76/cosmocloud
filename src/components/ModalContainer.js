import React, { useState } from "react";

function ModalContainer({ openDialog }) {
  const [data, setData] = useState([
    { key: 1, value: "Alice", type: "string" },
  ]);
  const types = [
    {
      id: "1",
      type: "String",
      value: "string",
    },
    {
      id: "2",
      type: "Integer",
      value: "int",
    },
    {
      id: "3",
      type: "Double",
      value: "double",
    },
  ];
  const temp = types.find((ext) => ext.value);

  const [currentType, setCurrentType] = useState(temp);

  const [newData, setNewData] = useState({ key: "", value: "", type: "" });
  const [addData, setAddData] = useState(false);
  const handleChange = (key, event) => {
    if (key === "key") setNewData({ ...newData, key: event.target.value });
    else if (key === "value")
      setNewData({ ...newData, value: event.target.value });
    else if (key === "type")
      setNewData({ ...newData, type: currentType.value });
    console.log(newData);
  };

  const handleChangeData = (key, event, index) => {
    if (key === "key") {
      data[index].key = event.target.value;
      setNewData({ data });
    } else if (key === "value") {
      data[index].value = event.target.value;
      setNewData({ data });
    } else if (key === "type") {
      data[index].type = event.value;
      setNewData({ data });
    }
    console.log(data);
  };
  const handleClose = () => setAddData(!addData);
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(newData);
    setData([...data, newData]);
    setNewData({ key: "", value: "", type: "" });
    console.log(newData);
  };
  return (
    <>
      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-auto my-6 mx-auto max-w-3xl">
          {/*content*/}
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            {/*header*/}
            <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
              <h3 className="text-3xl font-semibold">Modal Title</h3>
              <button
                className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                onClick={openDialog}
              >
                <span className="bg-transparent text-black  h-6 w-6 text-2xl block outline-none focus:outline-none">
                  x
                </span>
              </button>
            </div>
            {/*body*/}

            <div className="relative p-6 flex-auto">{/* body content */}</div>

            {/*footer*/}
            <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
              <button
                className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
                onClick={openDialog}
              >
                Close
              </button>
              <button
                className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
                onClick={openDialog}
              >
                Save Changes
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="opacity-30 fixed inset-0 z-40 bg-black"></div>
    </>
  );
}

export default ModalContainer;
