import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { Auth } from "aws-amplify";

function Confirmation() {
  const [code, setCode] = useState("");
  const navigate = useNavigate();
  const location = useLocation();
  const username = location.state.username;
  const confirmUser = async () => {
    console.log(username);
    try {
      const user = await Auth.confirmSignUp(username, code);
      console.log(user);
      console.log("USER VERIFIED");
    } catch (error) {
      console.log("error confirming sign up", error);
    }
    navigate("/", { replace: true });
  };

  const reSendCode = async () => {
    try {
      await Auth.resendSignUp(username);
      console.log("code resent successfully");
    } catch (err) {
      console.log("error resending code: ", err);
    }
  };
  return (
    <>
      <div className="flex items-center justify-center h-screen">
        <div className="p-4 rounded shadow-lg ring ring-indigo-600/50">
          <div className="flex flex-col items-center space-y-2">
            <h1 className="text-4xl font-bold">Enter Verifcation Code</h1>
            <p className="p-2">
              Enter the 6-digit verification code recieved on your e-mail.
            </p>
            <input
              id="code"
              name="code"
              type="number"
              autoComplete="number"
              required
              className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              placeholder="Verification Code"
              onChange={(e) => setCode(e.target.value)}
            ></input>
            <button
              onClick={confirmUser}
              className=" m-2 inline-flex items-center px-4 py-2 text-white bg-indigo-600 border border-indigo-600 rounded rounded-full hover:bg-indigo-700 focus:outline-none focus:ring"
            >
              <span className="text-sm font-medium">Submit</span>
            </button>
            <button
              onClick={reSendCode}
              className=" m-2 inline-flex items-center px-4 py-2 text-white bg-indigo-600 border border-indigo-600 rounded rounded-full hover:bg-indigo-700 focus:outline-none focus:ring"
            >
              <span className="text-sm font-medium">
                Re-Send Confimation Code
              </span>
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Confirmation;
