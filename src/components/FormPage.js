import React, { useState, useEffect } from "react";
import { Formik, Form, Field, FieldArray, ErrorMessage } from "formik";

function FormPage() {
  const initialValues = {
    name: "",
    fields: [
      { key: 10, dataType: "int" },
      { key: 10, dataType: "int" },
    ],
  };
  const types = [
    {
      id: "1",
      label: "String",
      value: "string",
    },
    {
      id: "2",
      label: "Integer",
      value: "int",
    },
    {
      id: "3",
      label: "Double",
      value: "double",
    },
  ];

  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(!open);
    const newObject = { key: "", dataType: "" };
    initialValues.push(newObject);
  };

  return (
    <div className="flex flex-row ">
      <div className="w-1/5 bg-slate-300 h-screen"></div>
      <div className="flex w-3/5 h-screen mt-10 ">
        <Formik
          initialValues={initialValues}
          validate={(values) => {
            const errors = {};
            return errors;
          }}
          onSubmit={(values) => {
            setTimeout(() => {
              //   alert(JSON.stringify(values, null, 2));
            }, 500);
          }}
        >
          {({ values }) => (
            <Form>
              <div className="flex flex-col ">
                <div className="flex justify-start ml-10 mb-10">
                  <Field
                    className="p-4 text-lg"
                    type="text"
                    name="name"
                    placeholder="Enter Modal Name"
                  />
                  {/* <ErrorMessage name="name" component="div" /> */}
                </div>
                <div className="flex flex-col ml-6 mt-2">
                  <div className="flex flex-row p-4">
                    <FieldArray name="fields">
                      {({ push }) => (
                        <div>
                          {values.fields && values.fields.length > 0 ? (
                            values.fields.map((field, index) => (
                              <div className="flex flex-row" key={index}>
                                <div className="=flex flex-col">
                                  <Field
                                    name={`fields.${index}.key`}
                                    type="text"
                                    placeholder="Enter a key"
                                  />
                                  <div className="p-4 inline">
                                    <Field
                                      className=""
                                      as="select"
                                      name={`fields.${index}.dataType`}
                                      //   value={field.dataType}
                                    >
                                      {types.map((type, index) => (
                                        <option
                                          key={index}
                                          value={type.value}
                                          label={type.label}
                                        >
                                          {" "}
                                          {type.label}
                                        </option>
                                      ))}
                                    </Field>
                                  </div>
                                </div>
                              </div>
                            ))
                          ) : (
                            <div></div>
                          )}
                        </div>
                      )}
                    </FieldArray>
                  </div>
                  {open && (
                    <div className="">
                      <FieldArray name="fields">
                        {({ push }) => (
                          <div>
                            <div
                              className="flex flex-row"
                              key={values.fields.length}
                            >
                              <div className="=flex flex-col">
                                <Field
                                  name={`fields.${values.fields.length}.key`}
                                  type="text"
                                  placeholder="Enter a key"
                                />
                                <div className="p-4 inline">
                                  <Field
                                    className=""
                                    as="select"
                                    name={`fields.${values.fields.length}.dataType`}
                                    //   value={field.dataType}
                                  >
                                    {types.map((type, index) => (
                                      <option
                                        key={index}
                                        value={type.value}
                                        label={type.label}
                                      >
                                        {/* {" "} */}
                                        {type.label}
                                      </option>
                                    ))}
                                  </Field>
                                </div>
                              </div>
                            </div>
                          </div>
                        )}
                      </FieldArray>
                      {/* hello */}
                    </div>
                  )}
                  <button className="bg-green-500 p-2" onClick={handleOpen}>
                    ADD
                  </button>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <div className="w-1/5 h-screen bg-slate-300"></div>
    </div>
  );
}

export default FormPage;
